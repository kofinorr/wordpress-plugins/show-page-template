��    	      d       �       �   =   �      �   
   �             
        )     <  �  X  P   �     =     Q     b     k     {     �     �   A simple plugin that show the page template in the page list. Default Template Front Page Kalimorr Page Template Posts Page Show Page Template https://github.com/Kalimorr Project-Id-Version: Show Page Template
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-08-30 12:27+0000
PO-Revision-Date: 2020-08-30 12:28+0000
Last-Translator: 
Language-Team: Français
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.3; wp-5.4.2 Un plugin simple pour afficher le nom du modèle de page utilisé dans la liste. Modèle par défaut Page d’accueil Kalimorr Modèle de page Page des articles Show Page Template https://github.com/Kalimorr 