<?php
/**
 * Plugin Name: Show Page Template
 * Description: A simple plugin that show the page template in the page list.
 * Version: 1.0.0
 * Author: Kalimorr
 * Author URI: https://gitlab.com/kalimorr/
 * Text Domain: krr-showpt
 * Domain Path: /languages
 * License: GPLv3 or later
 */

namespace KrrShowPageTemplate;

/**
 * Class ShowPageTemplate
 * @package KrrShowPageTemplate
 */
class ShowPageTemplate
{
	/**
	 * AdminMaintenance constructor.
	 */
	public function __construct()
	{
		add_action('plugins_loaded', [$this, 'load_i18n']);
		add_action('manage_page_posts_columns', [$this, 'manageColumns'], 15, 2);
		add_action('manage_page_posts_custom_column', [$this, 'manageRows'], 10, 2);
	}

	/**
	 * Load plugin textdomain.
	 */
	public function load_i18n() {
		load_plugin_textdomain(
			'krr-showpt',
			false,
			dirname(plugin_basename(__FILE__)) . '/languages'
		);
	}

	/**
	 * Add the column in the page list
	 * @param $columns
	 * @return mixed
	 */
	public function manageColumns($columns)
	{
		/* Get the needed data */
		$keys     = array_keys($columns);
		$index    = array_search('title', $keys);
		$position = (false === $index) ? count($columns) : $index + 1;

		/* Add the page template column after the title column */
		$columns = array_merge(
			array_slice( $columns, 0, $position ),
			['page_template' => __('Page Template', 'krr-showpt')],
			array_slice($columns, $position)
		);

		return $columns;
	}

	/**
	 * Render the template page name in a row
	 * @param $column
	 * @param $post_id
	 */
	public function manageRows($column, $post_id)
	{
		if ('page_template' === $column) {
			$templates    = wp_get_theme()->get_page_templates();
			$templateSlug = get_page_template_slug($post_id);

			if(intval( get_option( 'page_on_front' ) ) === $post_id) {
				_e( 'Front Page', 'krr-showpt');
			} elseif(intval( get_option( 'page_for_posts' ) ) === $post_id) {
				_e( 'Posts Page', 'krr-showpt');
			} elseif (isset($templates[$templateSlug])) {
				echo $templates[$templateSlug];
			} else {
				_e('Default Template', 'krr-showpt');
			}
		}
	}
}

new ShowPageTemplate();